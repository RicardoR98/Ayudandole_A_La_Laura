<?php

namespace App\Http\Controllers;

use App\Equipost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EquiposController extends Controller
{
    public function todosEquipos()
    {
        return Equipost::all();
    }

    public function guardarJugador($equipo, $jugador){

        DB::table('equipos')->insert([
            'nom_equipo' => $equipo,
            'jugador' => $jugador,
        ]);

        return 'ok';
    }

    public function eliminarJugador($id)
    {
        $jugador = Equipost::find($id);
        $jugador->delete();

        return 'ok';
    }

}
