package com.example.rodri.proyectooscar2;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, com.android.volley.Response.Listener<JSONObject>, com.android.volley.Response.ErrorListener{

    private EditText equipo, jugador;
    private Button enviar;

    private ListView lista;
    private ArrayAdapter<ListItem_Equipos> adapter = null;
    private ArrayList<ListItem_Equipos> equipos = new ArrayList<>();

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregarJugador();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        lista = (ListView) findViewById(R.id.List);

        obtener_jugadores();

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object fhater = (ListItem_Equipos) parent.getItemAtPosition(position);

                final String id_jugador = ((ListItem_Equipos)fhater).getId_jugador();


                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setTitle("Borrado de jugador");
                builder.setMessage("¿Decea eliminar a este jugador?");

                builder.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(MainActivity.this, "Accion Desecha", Toast.LENGTH_SHORT).show();

                            }
                        });

                builder.setPositiveButton("Si",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                eliminarJugador(id_jugador);

                            }
                        });
                builder.show();



            }
        });
    }

    private void eliminarJugador(String id) {
        request = Volley.newRequestQueue(MainActivity.this);

        String url = "http://192.168.1.90/ProyectoOscar/public/api/eliminarJugador/" + id;

        url = url.replace(" ", "%20");

        jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url,null, this, this);

        request.add(jsonObjectRequest);
    }

    private void agregarJugador() {
        Dialog d = new Dialog(this);
        d.setContentView(R.layout.agregarjugador);
        equipo = (EditText) d.findViewById(R.id.edt_equipo);
        jugador = (EditText) d.findViewById(R.id.edt_jugador);
        enviar = (Button) d.findViewById(R.id.btn_enviar);

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (equipo.getText().toString().equals("") || jugador.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "No se admiten campos vacios", Toast.LENGTH_SHORT).show();
                }else{
                    guardarJugador(equipo.getText().toString(), jugador.getText().toString());
                    Toast.makeText(MainActivity.this, equipo.getText().toString() + " " + jugador.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        d.show();
    }

    public void llenar(JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = jsonArray.getJSONObject(i);
            equipos.add(new ListItem_Equipos(object.getString("id"), object.getString("nom_equipo"), object.getString("jugador")));
        }
        adapter = new ArrayAdapter<ListItem_Equipos>(this, android.R.layout.simple_list_item_1, equipos);
        lista.setAdapter(adapter);
    }

    public void obtener_jugadores(){
        // Instantiate the RequestQueue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Prepare the Request
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET, // GET or POST
                "http://192.168.1.90/ProyectoOscar/public/api/todosEquipos", // URL
                null, // Parameters
                new Response.Listener<JSONArray>() { //Listener OK
                    @Override
                    public void onResponse(JSONArray responsePlaces) {
                        // Response OK!! :)
                        Log.v("VolleyPlacesRemoteDS",responsePlaces.toString());
                        try {
                            llenar(responsePlaces); // Mandar al metodo para adaptarlo a la lista
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() { // Listener ERROR

            @Override
            public void onErrorResponse(VolleyError error) {
                // There was an error :(
                Log.d("VolleyPlacesRemoteDS",error.toString());
            }
        });

        // Send the request to the requestQueue
        requestQueue.add(request);
    }


    private void guardarJugador(String equipo, String jugador) {
        request = Volley.newRequestQueue(MainActivity.this);

        String url = "http://192.168.1.90/ProyectoOscar/public/api/guardarJugador/" + equipo + "/" + jugador;

        url = url.replace(" ", "%20");

        jsonObjectRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url,null, this, this);

        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, " "+error, Toast.LENGTH_SHORT).show();
        Intent ListSong = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(ListSong);
    }

    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(this, "OK", Toast.LENGTH_SHORT).show();
        Intent ListSong = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(ListSong);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
